from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_todo,
    update_todo,
    delete_model_name,
    todo_item_item,
    edit_item,
)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", update_todo, name="update_todo"),
    path("<int:id>/delete/", delete_model_name, name="delete_model_name"),
    path("items/create", todo_item_item, name="todo_item_item"),
    path("items/<int:id>/item/", edit_item, name="edit_item"),
]
